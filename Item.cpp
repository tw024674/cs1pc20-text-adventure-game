#include "Item.h"

#include <iostream>

Item::Item(const std::string& name, const std::string& desc)
    : name(name), description(desc) {}

void Item::Interact() const {
  if (this->name == "Treasure Chest")
    std::cout << "You have gained 100 coins!\n";
  else if (this->name == "Excalibur Sword")
    std::cout << "You have gained a powerful sword\n";
  else if (this->name == "Potion")
    std::cout << "You start to feel strange effects from this potion\n";
  else if (this->name == "Gold Pouch")
    std::cout << "This pouch looks shiny! It might be worth something\n";
}

// Equality operator overload requried for std::remove which is used in
// Room::RemoveItem
bool Item::operator==(const Item& rhs) const {
  return this->name == rhs.name && this->description == rhs.description;
}

// Operator overload for cout to make printing Items easier and nicer
std::ostream& operator<<(std::ostream& os, const Item& item) {
  os << "Item Name: " << item.name << " | "
     << "Description: " << item.description;
  return os;
}

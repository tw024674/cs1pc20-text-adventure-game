#include <string>

#ifndef ITEM_H
#define ITEM_H

class Item {
 private:
  std::string name;
  std::string description;

 public:
  Item(const std::string& name, const std::string& desc);
  void Interact() const;
  bool operator==(const Item& rhs) const;
  friend std::ostream& operator<<(std::ostream& os, const Item& item);
  [[nodiscard]] inline std::string GetItemName() { return name; }
};

#endif  // ITEM_H

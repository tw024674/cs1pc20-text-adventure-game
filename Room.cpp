#include "Room.h"
#include <algorithm>
#include <iostream>

Room::Room(const std::string& name, const std::string& desc)
    : name(name), description(desc) {}

void Room::AddItem(Item item) {
  items.emplace_back(std::move(item));
}

// STL function that removes items from Vector
void Room::RemoveItem(const Item& item) {
  items.erase(std::remove(items.begin(), items.end(), item), items.end());
}

void Room::AddExit(const std::string& dir, Room* r) {
  exits[dir] = r;
}

// Describe the room in its entirerity
void Room::Explore() {
  std::cout << "You are in " << name << std::endl;
  std::cout << description << std::endl;

  std::cout << "This room has the following exits\n";
  for (auto const& [k, v] : exits) {
    std::cout << "Direction: " << k << " | "
              << "Name: " << v->GetName() << std::endl;
  }

  std::cout << "This room has the following items\n";
  for (const auto& i : items) {
    std::cout << i << std::endl;
  }
}

std::map<std::string, Room*> Room::GetExits() {
  return exits;
}

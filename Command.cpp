#include "Command.h"
#include <algorithm>
#include <iostream>
#include "util.h"

Command::Command(Player* _p) : p(_p) {}

void Command::Interpret(const std::string& command) const {
  std::vector<std::string> validCommands = {"look", "use", "move", "quit"};
  std::vector<std::string> splitCommand = split(command, ' ');

  // If the command cannot be found in the validCommands vector, it is not a
  // valid one
  if (splitCommand.empty() ||
      std::find(validCommands.begin(), validCommands.end(), splitCommand[0]) ==
          validCommands.end()) {
    std::cout << "Invalid command!\n";
  }

  // These following branches check which arguments have additional info
  // Some commands such as look obviously don't need additional arguments while
  // "move" and "use" do
  if (splitCommand[0] == "look") {
    if (splitCommand.size() > 1)
      std::cout << "Look does not take any arguments!\n";
    else
      p->GetLocation()->Explore();
  }

  else if (splitCommand[0] == "move") {
    if (splitCommand.size() < 2)
      std::cout << "Move requires an argument\n";
    else
      p->Move(splitCommand[1]);
  }

  else if (splitCommand[0] == "use") {
    if (splitCommand.size() < 2)
      std::cout << "Use requires an argument!\n";
    else
      for (auto& item : p->GetLocation()->GetItems()) {
        // Loop through all of the available items in a location
        if (splitCommand[1] == item.GetItemName()) {
          // If the name of the item provided by the user matches one in the
          // current room, use the item and then delete it
          item.Interact();
          p->GetLocation()->RemoveItem(item);
        } else
          std::cout << "That item is not available!\n";
      }
  }

  else if (splitCommand[0] == "quit") {
    if (splitCommand.size() > 1)
      std::cout << "Quit does not take any arguments!";
    else {
      std::cout << "Goodbye!\n";
      exit(0);
    }
  }
}

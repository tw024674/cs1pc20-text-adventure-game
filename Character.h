#include <string>
#include <vector>
#include "Item.h"
#include "Room.h"

#ifndef CHARACTER_H
#define CHARACTER_H

class Character {
 private:
  std::string name;
  int health;

 protected:
  std::vector<Item> inventory;

 public:
  Character(const std::string& name, int health);
  void TakeDamage(int damage);
};
class Player : protected Character {
 private:
  Room* location;

 public:
  Player(const std::string& name, int health, Room* location);
  [[nodiscard]] inline Room* GetLocation() const { return location; }
  void Move(const std::string& direction);
};

#endif  // CHARACTER_H

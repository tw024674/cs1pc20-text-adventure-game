#include <algorithm>
#include <sstream>
#include <string>
#include <vector>

#ifndef UTIL_H
#define UTIL_H

// helper function to split by delimiter
inline std::vector<std::string> split(const std::string& str,
                                      const char delimiter) {
  std::vector<std::string> tokens;
  std::istringstream stream(str);
  std::string token;
  while (getline(stream, token, delimiter)) {
    tokens.push_back(token);
  }
  return tokens;
}

// helper functions required for the trim function to work
// as we don't want anyone else accessing them, we hide them in a namespace
namespace lrtrim {
// trim left side whitespace
inline std::string ltrim(const std::string& s) {
  std::string result = s;
  result.erase(result.begin(),
               std::find_if(result.begin(), result.end(), [](unsigned char ch) {
                 return !std::isspace(ch);
               }));
  return result;
}

// right side
inline std::string rtrim(const std::string& s) {
  std::string result = s;
  result.erase(std::find_if(result.rbegin(), result.rend(),
                            [](unsigned char ch) { return !std::isspace(ch); })
                   .base(),
               result.end());
  return result;
}
}  // namespace lrtrim

// final user function
inline std::string trim(const std::string& s) {
  return lrtrim::ltrim(lrtrim::rtrim(s));
}

#endif  // UTIL_H

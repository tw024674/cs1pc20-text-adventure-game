#include <map>
#include <string>
#include <vector>
#include "Item.h"

#ifndef ROOM_H
#define ROOM_H

class Room {
 private:
  std::string name;
  std::string description;
  std::map<std::string, Room*> exits;
  std::vector<Item> items;

  // type alias to make it easier to type
  using ExitVec = std::vector<std::pair<std::string, Room*>>;

 public:
  Room(const std::string& name, const std::string& desc);
  void AddItem(Item item);
  void RemoveItem(const Item& item);
  void AddExit(const std::string& dir, Room* r);
  void Explore();
  [[nodiscard]] inline std::string GetName() { return name; }
  [[nodiscard]] std::map<std::string, Room*> GetExits();
  [[nodiscard]] inline std::vector<Item>& GetItems() { return items; }
};

#endif  // ROOM_H

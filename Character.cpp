#include "Character.h"
#include <iostream>

Character::Character(const std::string& name, int health)
    : name(name), health(health) {}

void Character::TakeDamage(int damage) {
  // Make the player die if the health is under 0 as result from damage taken
  this->health = health - damage;
  if (health <= 0) {
    std::cout << "Unfortunately, you have died!\n";
    exit(-1);
  }
}

Player::Player(const std::string& name, int health, Room* location)
    : Character(name, health), location(location) {}

void Player::Move(const std::string& direction) {
  // Check if there is an available exit in the direction request, if so, move
  // there
  if (auto exits = location->GetExits(); exits.count(direction))
    location = exits[direction];
  else
    std::cout << "Invalid direction or room!\n";
  // Inform the user if their choice was bad
}

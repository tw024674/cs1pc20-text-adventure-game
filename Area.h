#include "Room.h"

#ifndef AREA_H
#define AREA_H

class Area {
 private:
  std::map<std::string, Room*> rooms;

 public:
  void AddRoom(const std::string& name, Room* room);
  Room* GetRoom(const std::string& name);
  void ConnectRooms(const std::string& room1Name,
                    const std::string& room2Name,
                    const std::string& direction);
  void LoadMapFromFile(const std::string& filename);
};

#endif  // AREA_H

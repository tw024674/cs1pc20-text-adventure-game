#include "Character.h"

#ifndef COMMAND_H
#define COMMAND_H

class Command {
 private:
  Player* p;

 public:
  explicit Command(Player* _p);
  void Interpret(const std::string& command) const;
};

#endif  // COMMAND_H

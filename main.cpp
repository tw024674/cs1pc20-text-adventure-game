#include <iostream>

#include "Area.h"
#include "Command.h"
#include "util.h"

int main() {
  Area a;
  a.LoadMapFromFile("map.txt");

  // get the first room so that we can initialise the player properly
  auto p = new Player("Alan Koterba", 100, a.GetRoom("hallway"));
  auto c = Command(p);

  std::string command;
  while (true) {
    std::cout << ">  ";
    std::getline(std::cin, command);
    c.Interpret(trim(command));
  }

  return 0;
}

#include "Area.h"
#include "util.h"

#include <fstream>
#include <iostream>
#include <sstream>

void Area::AddRoom(const std::string& name, Room* room) {
  rooms[name] = room;
}

// Return the appropriate room pointer if it exists, otherwise return a null
// pointer
Room* Area::GetRoom(const std::string& name) {
  if (rooms.count(name))
    return rooms[name];
  else
    return nullptr;
}

// If both rooms exist in the map, add the exit
void Area::ConnectRooms(const std::string& room1Name,
                        const std::string& room2Name,
                        const std::string& direction) {
  if (rooms.count(room1Name) && rooms.count(room2Name))
    rooms[room1Name]->AddExit(direction, rooms[room2Name]);
}

void Area::LoadMapFromFile(const std::string& filename) {
  // check if file is valid, if not, exit program
  if (std::ifstream file(filename); !file.is_open()) {
    std::cout << "Filename is not valid!\n";
    exit(-4);
  }

  // Set up variables to read into
  std::ifstream file(filename);
  std::string line;
  std::vector<Room> rooms;
  std::vector<std::pair<std::string, std::string>> connectionData;

  while (std::getline(file, line)) {
    std::stringstream ss(line);
    std::string part;
    std::vector<std::string> parts;

    // Separate entire line by semicolons and insert contents into a vector
    while (std::getline(ss, part, ';'))
      parts.push_back(part);

    if (parts.size() >= 4) {
      // Add room name and description
      auto r = new Room(trim(parts[0]), trim(parts[1]));

      // Process items
      std::vector<std::string> itemParts = split(parts[3], ';');
      for (const auto& itemPart : itemParts) {
        std::vector<std::string> itemDetails = split(itemPart, '-');
        if (itemDetails.size() == 2) {
          r->AddItem(Item(trim(itemDetails[0]), trim(itemDetails[1])));
        }
      }

      this->AddRoom(trim(parts[0]), r);
      connectionData.emplace_back(trim(parts[0]), trim(parts[2]));
    }
  }

  // Process connections after all rooms have been added
  for (const auto& conn : connectionData) {
    std::vector<std::string> connections = split(conn.second, ' ');
    for (const auto& c : connections) {
      size_t delim = c.find('-');
      if (delim != std::string::npos) {
        std::string connRoomName = c.substr(0, delim);
        std::string connDir = c.substr(delim + 1);
        this->ConnectRooms(conn.first, trim(connRoomName), trim(connDir));
      }
    }
  }
}
